local mdxml = require "mdxml"

local a = [[
##version
###1.1
##encoding
###utf-8

`This is a list of programming languages and programming language books, in MDXML`

#programming
> #languages
> > #language
> > > #name
> > > > Lua
> > >
> > > #link
> > > > http://www.lua.org/
> >
> > #language
> > > #name
> > > > Python
> > >
> > > #link
> > > > https://www.python.org/
>
> # books
> > #book
> > ##edition
> > ###3
> > > #name
> > > > Programming in Lua
> > >
> > > #ISBN
> > > > 859037985X
]]

require 'pretty-print'.prettyPrint(mdxml.parse(a))

local b = [[
##version
###1.1
##encoding
###utf-8

`GLITCHMODE EXAMPLE
`

#  test  
> bleh
> # WE NEED TO GO DEEPER
>> ## ???
>> ### :3
>> heh
hey

## wat
### yeah
]]

require 'pretty-print'.prettyPrint(mdxml.parse(b))
